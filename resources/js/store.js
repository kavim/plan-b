import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { startCase } from 'lodash';
Vue.use(Vuex)

let plants = window.localStorage.getItem('plants');
let bees = window.localStorage.getItem('bees');
let months = window.localStorage.getItem('months');
let plant_image = window.localStorage.getItem('plant_image');

export default new Vuex.Store({
    state: {
      plants: plants ? JSON.parse(plants) : [],
      bees: bees ? JSON.parse(bees) : [],
      months: months ? JSON.parse(months) : [],
      resources: {
        plants: 0,
        months: 0,
        bees: 0
      },
      selectedMonths: [],
      selectedBees: [],
      plant_image: plant_image ? JSON.parse(plant_image) : {
        src: '/image/image-upload.png',
        data: '',
      },
    },
    getters: {
    },
    mutations:{
      store_plants(state, plants){
        state.plants = plants;
        state.resources.plants = 1;
      },
      store_bees(state, bees){
        state.bees = bees;
        state.resources.bees = 1;
      },
      store_months(state, months){
        state.months = months;
        state.resources.months = 1;
        window.localStorage.setItem('months', JSON.stringify(state.months));
      },
      set_image(state, image){
        state.plant_image = image;
      },      
      store_monthData(state, selectedMonts){
        state.selectedMonths = selectedMonts;
      },      
      store_beeData(state, bees){
        state.selectedBees = bees;
      }      
    },
    actions: {
      getPlants({ commit, dispatch  }) {
        axios.get('/api/get-plants/')
        .then(function (response) {
            commit('store_plants', response.data.plants);
            return true;
        })
        .catch(function (error) {
            console.log(error);
            console.log("DEU ERRO plants");
        });
      },
      getMonths({ commit }) {
        if(this.state.months.length > 0 ){
            this.state.resources.months = 1;
        }else{
            axios.get('/api/get-months/')
            .then(function (response) {
                commit('store_months', response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        }        
      },
      getBees({ commit }) {
        axios.get('/api/get-bees/')
        .then(function (response) {
            commit('store_bees', response.data);
        })
        .catch(function (error) {
            console.log(error);
        });
      },
      setMonthData({ commit }, selectedMonts){
        commit('store_monthData', selectedMonts);
      },
      setBeeData({ commit }, bees){
        commit('store_beeData', bees);
      },
      savePlants({ commit }, plants) {        
        commit('save_plants', plants);        
      },
      saveBees({ commit }, bees){
        commit('save_bees', bees);
      },
      setImage({ commit }, image){
        this.commit('set_image', image);
      }

    },

  })
