/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuex from 'vuex'
Vue.use(Vuex)
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Multiselect from 'vue-multiselect'

import store from './store.js';

import Home from './components/Home.vue'
import PlantRegister from './components/crud/Plant.vue'
import BeeRegister from './components/crud/Bee.vue'

Vue.component('plant-list', require('./components/PlantList.vue').default);
Vue.component('plant-view', require('./components/PlantView.vue').default);
Vue.component('filters', require('./components/Filters.vue').default);
Vue.component('plant-image', require('./components/crud/PlantImage.vue').default);
Vue.component('app-header', require('./components/AppHeader.vue').default);
Vue.component('register-header', require('./components/crud/RegisterHeader.vue').default);
Vue.component('register-bottom', require('./components/crud/RegisterBottom.vue').default);
Vue.component('multiselect', Multiselect);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/planta-cadastro',
            name: 'planta-cadastro',
            component: PlantRegister,
        },
        {
            path: '/abelha-cadastro',
            name: 'abelha-cadastro',
            component: BeeRegister,
        },
    ],
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    router
});
