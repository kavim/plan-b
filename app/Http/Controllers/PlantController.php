<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;
use SebastianBergmann\Environment\Console;

class PlantController extends Controller
{

    public function create(Request $request)
    {
        // \Log::info($request);

        $response = [
            'status'    => false,
            'response'  => "ok",
            'plant'     => []
        ];

        $validator = Validator::make($request->plant,
            [
                'name' => ['required', 'max:200'],
                'species' => 'required|min:0|max:200',
                'description' => 'required|min:0|max:200',
                'selectedMonts.*' => 'required|integer|min:1',
                'selectedBees.*' => 'required|integer|min:1',
            ]
        );

        if ($validator->fails()) {
            
            $response['response'] = $validator->getMessageBag();
            return $response;
        }

        $plant = $this->storePlant($request->plant);

        $plantMonths = $this->storePlantMonths($request->selectedMonts, $plant->id);

        $plantBees = $this->storePlantBees($request->selectedBees, $plant->id);

        if($plant && $plantMonths && $plantBees){
            $response['status'] = true;
            $response['plant'] = $plant;
        }       

        return $response;
    }

    public function storePlant($request)
    {
        $new_plant = false;

        $new_plant = \App\Plant::create(
            [
                'name' => $request['name'],
                'species' => $request['species'],
                'description' => $request['description'],
            ]
        );

        return $new_plant;
    }
    
    public function storePlantMonths($selectedMonts, $plant_id)
    {
        try {
            
            foreach ($selectedMonts as $value) {
            
                \App\PlantMonths::create([
                    'month_id'  => $value,
                    'plant_id'  => $plant_id
                ]);
    
            }

            return true;

        } catch (\Throwable $e) {
            report($e);    
            return false;
        }
    }

    public function storePlantBees($selectedBees, $plant_id)
    {
        try {
            
            foreach ($selectedBees as $value) {
            
                \App\PlantBee::create([
                    'bee_id'  => $value,
                    'plant_id'  => $plant_id
                ]);
    
            }

            return true;

        } catch (\Throwable $e) {
            report($e);    
            return false;
        }
    }

    public function saveImage(Request $request)
    {
        $request['data'] = json_decode($request['data']);

        $plant_id = $request['data']->plant_id;

        if(!$request->hasFile('image') && !$this->validate($request, ['image' => 'image|max:20480',])) {
            return [
                'status' => false
            ];
        }

        $src = $request->file('image')->store("image/" . Carbon::now()->format('Y-m-d'), 'public');
        
        $img = \App\PlantImage::create([
            'src' => $src,
            'plant_id' => $plant_id
        ]);

        $image = Image::make(storage_path("app/public/".$src))->fit(500, 500)->save();
        $image->save();

        return [
            'status' => true,
            'src' => "/storage/".$img->src
        ];
    }

}
