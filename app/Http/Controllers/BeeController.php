<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BeeController extends Controller
{
    public function create(Request $request)
    {
        // \Log::info($request);

        $response = [
            'status'    => false,
            'response'  => "Erro ao cadastrar Abelha",
            'bee'     => []
        ];

        $validator = Validator::make($request->bee,
            [
                'name' => ['required', 'max:200'],
                'species' => 'required|min:0|max:200',
            ]
        );

        if ($validator->fails()) {
            
            $response['response'] = $validator->getMessageBag();
            return $response;
        }

        $bee = $this->store($request->bee);

        if($bee){
            $response['status'] = true;
            $response['response'] = "ok";
            $response['bee'] = $bee;
        }

        return $response;
    }

    public function store($request)
    {
        $bee = false;

        $bee = \App\Bee::create(
            [
                'name' => $request['name'],
                'species' => $request['species'],
            ]
        );

        return $bee;
    }
}
