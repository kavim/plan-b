<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Months;
use \App\Bee;
use App\Plant;

class SpaController extends Controller
{
    public function index()
    {
        return view('index');
    }
    
    public function getMonths()
    {
        $months =  Months::get();
        return $months->makeHidden(['created_at', 'updated_at'])->toArray();
    }
    
    public function getBees()
    {        
        $bees = Bee::orderByDesc('id')->get();
        return $bees->makeHidden(['created_at', 'updated_at'])->toArray();
    }
    
    public function getPlants()
    {
        $plants = array();
        $return = [
            'status' => false
        ];
        
        $theplants =  Plant::get();
    
        foreach ($theplants as $key => $value) {
            array_push($plants, 
                    [
                        'name' => $value->name,
                        'species' => $value->species,
                        'description' => $value->description,
                        'bees' => $value->getPlant_bees(),
                        'months' => $value->getPlant_months(),
                        'src' => $value->getPlant_image(),
                    ]
                );
        }

        $return = [
            'status'    => false,
            'plants'   => $plants
        ];

        return $return;
    }
}
