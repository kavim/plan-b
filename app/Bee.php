<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bee extends Model
{
    protected $fillable = ['name', 'species'];
}
