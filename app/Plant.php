<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $fillable = ['name', 'species', 'description'];


    public function getPlant_bees()
    {
        $re = $this->hasMany(\App\PlantBee::class, 'plant_id');

        return $re->pluck('bee_id');
    }

    public function getPlant_months()
    {
        $re = $this->hasMany(\App\PlantMonths::class, 'plant_id');

        return $re->pluck('month_id');
    }
    
    public function getPlant_image()
    {
        $image = PlantImage::where('plant_id', $this->id)->orderBy("id", "DESC")->first();

        return $image ? '/storage/'.$image->src : '/image/default.png';
    }
}
