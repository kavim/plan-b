<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantImage extends Model
{
    protected $fillable = ['src', 'plant_id'];
}
