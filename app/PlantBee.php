<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantBee extends Model
{
    protected $fillable = ['plant_id', 'bee_id'];
}
