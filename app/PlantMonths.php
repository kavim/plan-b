<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantMonths extends Model
{
    protected $fillable = ['month_id', 'plant_id'];
}
