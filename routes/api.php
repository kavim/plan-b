<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('get-months', 'SpaController@getMonths');
Route::get('get-plants', 'SpaController@getPlants');
Route::get('get-bees', 'SpaController@getBees');

Route::post('create-plant', 'PlantController@create');
Route::post('create-image', 'PlantController@saveImage');
Route::post('create-bee', 'BeeController@create');
