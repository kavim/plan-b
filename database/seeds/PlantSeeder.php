<?php

use Illuminate\Database\Seeder;

class PlantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Plant::create([
            'name' => 'planta 1',
            'species' => 'desconhecida',
            'description' => 'uma descrição bem masssaaaa!',
        ]);

        \App\PlantBee::create(
            [
                'plant_id'  => 1,
                'bee_id'    => 1
            ]
        );

        \App\PlantMonths::create(
            [
                'plant_id'  => 1,
                'month_id'    => 1
            ]
        );
        
        \App\PlantMonths::create(
            [
                'plant_id'  => 1,
                'month_id'    => 2
            ]
        );

    }
}
