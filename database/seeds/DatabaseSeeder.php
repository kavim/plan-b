<?php

use App\PlatImage;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MonthSeeder::class,
            BeeSeeder::class,
            PlantSeeder::class,
            // PlantImageSeeder::class,
        ]);
    }
}
