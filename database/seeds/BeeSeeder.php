<?php

use Illuminate\Database\Seeder;

class BeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $bees = [
            [
                'name' => 'Uruçu',
                'species' => 'Melipona scutellaris',
            ],
            [
                'name' => 'Uruçu-Amarela',
                'species' => 'Melipona rufiventris',
            ],
            [
                'name' => 'Guarupu',
                'species' => 'Melipona bicolor',
            ],
            [
                'name' => 'Iraí',
                'species' => 'Nannotrigona testaceicornes',
            ],
            [
                'name' => 'Jataí',
                'species' => 'Tetragonisca angustula',
            ],
            [
                'name' => 'Jataí-da-Terra',
                'species' => 'Paratrigona subnuda',
            ],
            [
                'name' => 'Mandaçaia',
                'species' => 'Melipona mandaçaia',
            ],
            [
                'name' => 'Manduri',
                'species' => 'Melipona marginata',
            ],
            [
                'name' => 'Tubuna',
                'species' => 'Scaptotrigona bipunctata',
            ],
            [
                'name' => 'Mirim Droryana',
                'species' => 'Plebeia droryana',
            ],
            [
                'name' => 'Mirim-Guaçu',
                'species' => 'Plebeia remota',
            ],
            [
                'name' => 'Mirim-Preguiça',
                'species' => 'Friesella Schrottkyi',
            ],
            [
                'name' => 'Lambe-Olhos',
                'species' => 'Leurotrigona muelleri',
            ],
            [
                'name' => 'Borá',
                'species' => 'Tetragona clavipes',
            ],
            [
                'name' => 'Boca-de-Sapo',
                'species' => 'Partamona helleri',
            ],
            [
                'name' => 'Guira',
                'species' => 'Geotrigona mombuca',
            ],
            [
                'name' => 'Marmelada Amarela',
                'species' => 'Frieseomelitta varia',
            ],
            [
                'name' => 'Mombucão',
                'species' => 'Cephalotrigona capitata',
            ],
            [
                'name' => 'Guiruçu',
                'species' => 'Schwarziana quadripunctata',
            ],
            [
                'name' => 'Tataíra',
                'species' => 'Oxytrigona tataira tataira',
            ],
            [
                'name' => 'Irapuã',
                'species' => 'Trigona spinipes',
            ],
            [
                'name' => 'Abelha-Limão',
                'species' => 'Lestrimelitta limao',
            ],
            [
                'name' => 'Bieira',
                'species' => 'Mourella caerulea',
            ],
        ];

        foreach ($bees as $b){
            \App\Bee::create([
                'name' => $b['name'],
                'species' => $b['species'],
            ]);
        }
    }
}
