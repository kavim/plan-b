<?php

use Illuminate\Database\Seeder;

class PlantImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\PlantImage::create([
            'src'       => 'default.jpg',
            'plant_id'  => 1
        ]);
    }
}
